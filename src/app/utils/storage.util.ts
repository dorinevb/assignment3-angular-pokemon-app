export class StorageUtil {
  public static saveToStorage<T>(key: string, value: T): void {
    sessionStorage.setItem(key, JSON.stringify(value));
  }
  
  public static readFromStorage<T>(key: string): T | undefined {
    
    try {
      const storedJSON = sessionStorage.getItem(key);
      if (storedJSON) {
        return JSON.parse(storedJSON);
      }
      return undefined;
    } catch(error) {
      sessionStorage.removeItem(key);
      return undefined;
    }
  }
}


