import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss']
})
export class LoginPage {

  get loading(): boolean {
    return this.loginService.loading;
  }

  constructor(
    private readonly loginService: LoginService,
    private readonly router: Router
  ) { }

  handleLogin(): void {
    this.router.navigateByUrl("/catalogue");
  }

}
