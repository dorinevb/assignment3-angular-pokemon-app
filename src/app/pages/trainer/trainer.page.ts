import { Component, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { Trainer } from 'src/app/models/trainer.model';
import { CatalogueService } from 'src/app/services/catalogue.service';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.page.html',
  styleUrls: ['./trainer.page.scss']
})
export class TrainerPage implements OnInit {

  get trainer(): Trainer | undefined {
    return this.trainerService.trainer;
  }

  get trainerPokemon(): Pokemon[] {
    return this.trainer?.pokemon!;
  }

  get trainerName(): string {
    return this.trainer?.username!;
  }

  get pokemon(): Pokemon[] {
    return this.catalogueService.pokemon;
  }

  constructor(
    private readonly trainerService: TrainerService,
    private readonly catalogueService: CatalogueService
  ) { }

  ngOnInit(): void {
  }

}
