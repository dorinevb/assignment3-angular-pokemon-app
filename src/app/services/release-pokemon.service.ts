import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize, Observable, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pokemon } from '../models/pokemon.model';
import { Trainer } from '../models/trainer.model';
import { CatalogueService } from './catalogue.service';
import { TrainerService } from './trainer.service';

const { apiKey, apiTrainers } = environment;

@Injectable({
  providedIn: 'root'
})
export class ReleasePokemonService {

  private _loading: boolean = false;

  get loading(): boolean {
    return this._loading;
  }

  constructor(
    private http: HttpClient,
    private readonly catalogueService: CatalogueService,
    private readonly trainerService: TrainerService
  ) { }
  
  // remove a pokemon from the collection of the trainer
  public removeFromCollection(pokemonId: string): Observable<Trainer> {
    if (!this.trainerService.trainer) {
      throw new Error("No trainer to remove pokemon from");
    }
    const trainer: Trainer = {...this.trainerService.trainer};
    const poke: Pokemon | undefined = this.catalogueService.pokemonById(pokemonId);
    if (!poke) {
      throw new Error(`releaseFromCollection: No pokemon with id: ${pokemonId}`);
    }
    if (!this.trainerService.inCollection(pokemonId)) {
      throw new Error("removeFromCollection: pokemon not in collection");
    }

    this._loading = true;

    const headers = new HttpHeaders({
      'content-type': 'application/json',
      'x-api-key': apiKey
    });

    return this.http.patch<Trainer>(`${apiTrainers}/${trainer.id}`, {
      pokemon: [...trainer.pokemon.filter((p) => p.id !== poke.id)]
    }, {
      headers
    })
    .pipe(
      tap((updatedTrainer: Trainer) => {
        this.trainerService.trainer = updatedTrainer;
      }),
      finalize(() => this._loading = false)
    )
  }

}
