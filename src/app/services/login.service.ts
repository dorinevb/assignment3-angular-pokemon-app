import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize, map, Observable, of, switchMap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Trainer } from '../models/trainer.model';

const urlTrainers = environment.apiTrainers;
const apiKey = environment.apiKey;

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private _loading: boolean = false;

  get loading(): boolean {
    return this._loading;
  }

  constructor(private readonly http: HttpClient) { }

  // Log in trainer
  public loginTrainer(username: string): Observable<Trainer> {
    this._loading = true;
    return this.checkTrainer(username)
      .pipe(
        switchMap((trainer: Trainer | undefined) => {
          if (trainer !== undefined) {
            return of(trainer);
          }
          return this.createTrainer(username);
        }),
        finalize(() => this._loading = false)
      )
  }

  // Check if trainer already exists
  private checkTrainer(username: string): Observable<Trainer | undefined> {
    return this.http.get<Trainer[]>(`${urlTrainers}?username=${username}`)
      .pipe(
        map((response: Trainer[]) => response.pop())
      );
  }

  // Create a new trainer
  private createTrainer(username: string): Observable<Trainer> {
    const trainer = {
      username,
      pokemon: []
    };
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-api-key": apiKey,
    });
    return this.http.post<Trainer>(urlTrainers, trainer, {
      headers
    });
  }

}
