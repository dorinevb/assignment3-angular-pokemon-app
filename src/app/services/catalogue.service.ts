import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { PokemonArray } from '../models/pokemon-array.model';
import { Pokemon } from '../models/pokemon.model';
import { StorageUtil } from '../utils/storage.util';
import { StorageKeys } from '../enums/storage-keys.enum';

const { apiPokemon } = environment;

@Injectable({
  providedIn: 'root'
})
export class CatalogueService {

  private _pokemon: Pokemon[] = [];
  private _error: string = "";
  private _loading: boolean = false;
  /* Icon by IconMarketPK - Flaticon */
  private _notFoundImage: string = "https://cdn-icons.flaticon.com/png/512/4068/premium/4068026.png?token=exp=1651227633~hmac=5ab751c3836abcb455859843bb72cd23";

  // Generate id and image url / 'not found image' url
  public getIdAndImage(pokemonItem: Pokemon): string[] {
    try {
      const id = pokemonItem.url.split("/").filter(x => x !== "").pop()!;
      const url = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`
      return [id, url];
    } catch (error) {
      const id = (Math.random()*100).toString();
      const url = this._notFoundImage;
      return [id, url];
    }
  }

  get pokemon(): Pokemon[] {
    let pokemonInStorage = StorageUtil.readFromStorage<Pokemon[]>(StorageKeys.Pokemon);
    if (pokemonInStorage !== undefined) {
      return pokemonInStorage;
    } else {
      return [];
    }
  }

  get error(): string {
    return this._error;
  }

  get loading(): boolean {
    return this._loading;
  }

  constructor(private readonly http: HttpClient) { }

  public fetchAllPokemon(): void {
    // If pokemon are already loaded in sessionStorage, save in pokemon variable and return
    const currentPokemon: Pokemon[] | undefined = StorageUtil.readFromStorage(StorageKeys.Pokemon);
    if ((currentPokemon !== undefined) && (currentPokemon.length > 0)) {
      this._pokemon = currentPokemon;
      return;
    }
    // Otherwise fetch pokemon from the api
    this._loading = true;
    this.http.get<PokemonArray>(apiPokemon)
      .subscribe({
        next: (pokemon: PokemonArray) => {
          let pokemonForStorage = pokemon.results.map((aPokemon) => {
            const [ id, url ] = this.getIdAndImage(aPokemon);
            return {
            ...aPokemon, 
            id: id,
            imageUrl: url
            };
          });
          StorageUtil.saveToStorage<Pokemon[]>(StorageKeys.Pokemon, pokemonForStorage);
          this._pokemon = pokemonForStorage;
        },
        error: (error: HttpErrorResponse) => {
          this._error = error.message;
        },
        complete: () => {
          this._loading = false;
        }
      })
  }

  public pokemonById(id: string): Pokemon | undefined {
    return this._pokemon.find((poke: Pokemon) => poke.id === id);
  }

}
