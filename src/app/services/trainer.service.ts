import { Injectable } from '@angular/core';
import { StorageKeys } from '../enums/storage-keys.enum';
import { Pokemon } from '../models/pokemon.model';
import { Trainer } from '../models/trainer.model';
import { StorageUtil } from '../utils/storage.util';

@Injectable({
  providedIn: 'root'
})
export class TrainerService {

  private _trainer?: Trainer;

  get trainer(): Trainer | undefined {
    return this._trainer;
  }

  set trainer(trainer: Trainer | undefined) {
    StorageUtil.saveToStorage<Trainer>(StorageKeys.Trainer, trainer!);
    this._trainer = trainer;
  }

  constructor() {
    this._trainer = StorageUtil.readFromStorage<Trainer>(StorageKeys.Trainer);
  }

  // Check if pokemon is in collection of trainer
  public inCollection(pokemonId: string): boolean {
    if (this._trainer) {
      return Boolean(this.trainer?.pokemon.find((poke: Pokemon ) => poke.id === pokemonId));
    }
    return false;
  }
}
