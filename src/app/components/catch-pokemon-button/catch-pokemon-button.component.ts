import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { Trainer } from 'src/app/models/trainer.model';
import { CatchPokemonService } from 'src/app/services/catch-pokemon.service';
import { ReleasePokemonService } from 'src/app/services/release-pokemon.service';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-catch-pokemon-button',
  templateUrl: './catch-pokemon-button.component.html',
  styleUrls: ['./catch-pokemon-button.component.scss']
})
export class CatchPokemonButtonComponent implements OnInit {

  public isCaught: boolean = false;
  @Input() pokemonId: string = "";

  get loading(): boolean {
    return this.catchPokemonService.loading;
  }

  constructor(
    private readonly trainerService: TrainerService,
    private readonly catchPokemonService: CatchPokemonService,
    private readonly releasePokemonService: ReleasePokemonService
  ) { }

  ngOnInit(): void {
    this.isCaught = this.trainerService.inCollection(this.pokemonId);
  }

  // Add pokemon to trainer collection
  onCatch(): void {
    this.catchPokemonService.addToCollection(this.pokemonId)
      .subscribe({
        next: (response: Trainer) => {
          console.log("response", response);
          this.isCaught = true;
        },
        error: (error: HttpErrorResponse) => {
          console.log(error.message);
        }
      })
  }

  // Remove pokemon from trainer collection
  onRelease(): void {
    this.releasePokemonService.removeFromCollection(this.pokemonId)
      .subscribe({
        next: (response: Trainer) => {
          console.log("response", response);
          this.isCaught = false;
        },
        error: (error: HttpErrorResponse) => {
          console.log(error.message);
        }
      })
  }
}
