import { Component, Input, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';

@Component({
  selector: 'app-collected-pokemon',
  templateUrl: './collected-pokemon.component.html',
  styleUrls: ['./collected-pokemon.component.scss']
})
export class CollectedPokemonComponent implements OnInit {

  @Input() trainerPokemon: Pokemon[] = [];
  @Input() trainerName: string = "";
  @Input() pokemon: Pokemon[] = [];

  constructor() { }

  ngOnInit(): void {
  }

}
