import { Pokemon } from "./pokemon.model";

export interface PokemonArray {
  count: number;
  next: string;
  previous: string;
  results: Pokemon[];
}