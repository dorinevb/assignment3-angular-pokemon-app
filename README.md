# NgPokemonApp

This Pokemon App was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.3.3. The app allows users to be a pokemon trainer and 'catch' and 'release' the pokemon that are available in the app catalogue. It fetches pokemon data from the Poke Api (https://pokeapi.co/) and pokemon avatar from a GitHub repository (https://github.com/PokeAPI/sprites). The app temporarily stores user data (username and collected pokemon) in an API on Heroku.

This GitLab repository contains:    

1. The source code written in Typescript, html and scss, using the Angular framework.
2. A pdf-file with the component tree, which shows the overall structure of the app.

## Background

I created this Angular app as an assignment for the frontend course of Noroff School of Technology and Digital Media. The course is part of the software development traineeship at Experis (Manpower Group).

## Install

This project uses node and npm. Please make sure you have them installed.
Also make sure to install the Angular CLI by running `npm install -g @angular/cli`.
After downloading the source code, run `npm install` to install the required node packages for this app.
Run `ng serve -o` to start up the development server. The app will open in your browser on `http://localhost:4200/`.

You can also try out the app on Heroku: `https://afternoon-hollows-11746.herokuapp.com/login`.

## Usage

This app consists of three views:
1. The Login View: here you can enter your username (min. 3 characters) and log in. The app checks if the user is already in the api and otherwise creates a new user.
2. The Catalogue View: here you see the collection of available pokemon, on cards with their name and avatar. By clicking on the red 'catch!' button you can 'catch' a pokemon. By clicking on the blue 'release' button you can release it again.
3. The Trainer View: here you can see the pokemon that you have caught as a pokemon trainer. By clicking on the blue 'release' button of a pokemon, it will disappear from the Trainer View. The pokemon can then be caught again in the Catalogue View. 

The app also has a navbar, which is displayed in the top of the page. When the user is logged in, the navbar shows the links to navigate between the Catalogue View and the Trainer View, as well as a Log Out button. 

## License

MIT

Copyright (c) 2022 Dorine van Belzen

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
